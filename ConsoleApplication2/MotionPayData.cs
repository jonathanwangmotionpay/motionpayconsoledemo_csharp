﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Net;
using System.Security.Cryptography;
using Newtonsoft.Json;

namespace MotionPayConsoleDemo
{
    /*
     * the sample request
        "param":
        {
            "amount":1,
            "authCode":"134583657133882637",
            "merchantOrderNo":"2018081106176844",
            "paramJsonObject":
            {
                "goods_info":"Test_Product",
                "spbill_create_ip":"192.168.2.253",
                "store_id":"",
                "terminal_no":"MyPosT00001"
            },
            "payChannel":"U"
        },
        "signature":"57351cedc47a8aa71b5405606148f96b6b44974e",
        "suffix":
        {
            "mid":"100100010000010"
        }
    */
    // request classes
    class ParamJsonObject
    {
        public string goods_info
        {
            get;
            set;
        }
        public string spbill_create_ip
        {
            get;
            set;
        }
        public string store_id
        {
            get;
            set;
        }
        public string terminal_no
        {
            get;
            set;
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
    class Suffix
    {
        public string mid
        {
            get;
            set;
        }
    }
    class BaseParam
    {
        public string GetQueryString()
        {
            var properties = from p in this.GetType().GetProperties()
                             where p.GetValue(this, null) != null
                             select p.Name.ToLower() + "=" + p.GetValue(this, null).ToString();

            return String.Join("&", properties.ToArray());
        }
    }
    class Param : BaseParam
    {
        public int amount
        {
            get;
            set;
        }
        public string authCode
        {
            get;
            set;
        }
        public string merchantOrderNo
        {
            get;
            set;
        }
        public ParamJsonObject paramJsonObject
        {
            get;
            set;
        }
        public string payChannel
        {
            get;
            set;
        }
    }
    class OrderRequest
    {
        public Param param
        {
            get;
            set;
        }
        public string signature
        {
            get;
            set;
        }
        public Suffix suffix
        {
            get;
            set;
        }
    }
    class QueryParam : BaseParam
    {
        public string merchantOrderNo
        {
            get;
            set;
        }
    }
    class QueryRequest
    {
        public QueryParam param
        {
            get;
            set;
        }
        public Suffix suffix
        {
            get;
            set;
        }
        public string signature
        {
            get;
            set;
        }
    }

    class RefundCancelParam : BaseParam
    {
        public string orderNo
        {
            get;
            set;
        }
        public int refundAmount
        {
            get;
            set;
        }
        public string tranCode
        {
            get;
            set;
        }
        public string tranLogId
        {
            get;
            set;
        }
    }
    class RefundCancelRequest
    {
        public RefundCancelParam param
        {
            get;
            set;
        }
        public Suffix suffix
        {
            get;
            set;
        }
        public string signature
        {
            get;
            set;
        }
    }

    // response classes
    class OrderDef
    {
        public int amount
        {
            get;
            set;
        }
        public int cnyAmount
        {
            get;
            set;
        }
        public float exchangeRate
        {
            get;
            set;
        }
        public string merchantOrderNo
        {
            get;
            set;
        }
        public string mnFlag
        {
            get;
            set;
        }
        public string orderNo
        {
            get;
            set;
        }
        public string payTime
        {
            get;
            set;
        }
        public string payType
        {
            get;
            set;
        }
        public int refundAmount
        {
            get;
            set;
        }
        public string sn
        {
            get;
            set;
        }
        public int state
        {
            get;
            set;
        }
        public int tipAmount
        {
            get;
            set;
        }
        public string tranCode
        {
            get;
            set;
        }
        public string tranLogId
        {
            get;
            set;
        }
        public string utcTimes
        {
            get;
            set;
        }
    }
    class Result
    {
        public string err_code
        {
            get;
            set;
        }
        public OrderDef orderDef
        {
            get;
            set;
        }
    }
    class Response
    {
        public string code
        {
            get;
            set;
        }
        public string message
        {
            get;
            set;
        }
        public Result result
        {
            get;
            set;
        }
    }
    class OtherResponse
    {
        public string code
        {
            get;
            set;
        }
        public string message
        {
            get;
            set;
        }
        public OrderDef result
        {
            get;
            set;
        }
    }
    class MotionPayData
    {
        private string GetSignParams(MotionPayAPI theMotionPayAPI)
        {
            return "&appid=" + theMotionPayAPI.getAppId() + "&appsecret=" + theMotionPayAPI.getAppSecure();
        }

        private string GetSHA1HashData(string data)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            byte[] hashData = sha.ComputeHash(Encoding.UTF8.GetBytes(data));

            return BitConverter.ToString(hashData).Replace("-", "").ToLower();
        }

        public string getSign(BaseParam theParam, MotionPayAPI theMotionPayAPI)
        {
            //the stringSignTemp Should be: amount=1&authcode=134583657133882637&merchantorderno=2018081106176844&paramjsonobject={"goods_info":"Test_Product","spbill_create_ip":"192.168.2.253","store_id":"","terminal_no":"MyPosT00001"}&paychannel=U&appid=5005642017008&appsecret=cd3f5e88a1ec1df2351cdca75d7ce94a
            string stringSignTemp = theParam.GetQueryString() + this.GetSignParams(theMotionPayAPI);
            Console.WriteLine("stringSignTemp is:" + stringSignTemp);
            // string sign = GetSHA1HashData("amount=1&authcode=134583657133882637&merchantorderno=2018081106176844&paramjsonobject={\"goods_info\":\"Test_Product\",\"spbill_create_ip\":\"192.168.2.253\",\"store_id\":\"\",\"terminal_no\":\"MyPosT00001\"}&paychannel=U&appid=5005642017008&appsecret=cd3f5e88a1ec1df2351cdca75d7ce94a");
            string sign = GetSHA1HashData(stringSignTemp);
            Console.WriteLine("sign is:" + sign);
            return sign;
        }

        public string GetPaymentStateString(int paymentState) 
        {
            string stateString = "";
            if (paymentState > 0)
            {
                if (paymentState == 1)
                {
                    stateString = paymentState + " - Paying";
                }
                if (paymentState == 2)
                {
                    stateString = paymentState + " - Paid";
                }
                if (paymentState == 3)
                {
                    stateString = paymentState + " - Refund";
                }
                if (paymentState == 4)
                {
                    stateString = paymentState + " - Closed";
                }
                if (paymentState == 5)
                {
                    stateString = paymentState + " - Cancelled";
                }
            }
            return stateString;
        }

    }
}
