﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Web;
using System.Net;
using System.Security.Cryptography;
using Newtonsoft.Json;

namespace MotionPayConsoleDemo
{

    class MotionPayAPI
    {
        private bool usingLiveServer = false;
        private string API_HOST_URL_TESTING = "https://api.motionpay.org/";
        private string API_HOST_URL_LIVE = "APIHOST_To_Be_Replaced_Before_Live";

        private string mid_TEST = "100100010000010";
        private string appId_TEST = "5005642017008";
        private string appSecure_TEST = "cd3f5e88a1ec1df2351cdca75d7ce94a";

        private string mid_LIVE = "MID_To_Be_Replaced_Before_Live";
        private string appId_LIVE = "APPID_To_Be_Replaced_Before_Live";
        private string appSecure_LIVE = "APPSECURE_To_Be_Replaced_Before_Live";

        /**
          * offline POS machine API action target for order
          */
        private string ACTION_API_ORDER = "/payment/pay/order";
        /**
         * offline POS machine API action target for query
         */
        private string ACTION_API_QUERY_ORDER = "/payment/pay/queryOrder";
        /**
         * offline POS machine API action target for refund
         */
        private string ACTION_API_REVOKE = "/payment/pay/revoke";
        /**
         * offline POS machine API action target for cancel
         */
        private string ACTION_API_CANCEL = "/payment/pay/cancel";

        public void SetUsingLiveServerFlag(bool usingLive)
        {
            usingLiveServer = usingLive;
        }

        private string GetServerApiHostURL()
        {
            string url = API_HOST_URL_TESTING;
            if (usingLiveServer)
            {
                url = API_HOST_URL_LIVE;
            }
            return url;
        }

        private string GetOrderServerActionURL()
        {
            return GetServerApiHostURL() + ACTION_API_ORDER;
        }

        private string GetQueryServerActionURL()
        {
            return GetServerApiHostURL() + ACTION_API_QUERY_ORDER;
        }

        private string GetRevokeServerActionURL()
        {
            return GetServerApiHostURL() + ACTION_API_REVOKE;
        }

        private string GetCancelServerActionURL()
        {
            return GetServerApiHostURL() + ACTION_API_CANCEL;
        }

        public string getMid()
        {
            string mid = mid_TEST;
            if (usingLiveServer)
            {
                mid = mid_LIVE;
            }
            return mid;
        }

        public string getAppId()
        {
            string appId = appId_TEST;
            if (usingLiveServer)
            {
                appId = appId_LIVE;
            }
            return appId;
        }

        public string getAppSecure()
        {
            string appSecure = appSecure_TEST;
            if (usingLiveServer)
            {
                appSecure = appSecure_LIVE;
            }
            return appSecure;
        }

        private string PostData(string uri, string jsonRequest)
        {
            var http = (HttpWebRequest)WebRequest.Create(new Uri(uri));
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";

            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] bytes = encoding.GetBytes(jsonRequest);

            Stream newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();

            var response = http.GetResponse();

            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();

            return content;
        }

        public string SendOrderRequest(string jsonRequest)
        {
            string theResult = PostData(GetOrderServerActionURL(), jsonRequest);
            return theResult;
        }

        public string SendQueryRequest(string jsonRequest)
        {
            string theResult = PostData(GetQueryServerActionURL(), jsonRequest);
            return theResult;
        }

        public string SendRefundRequest(string jsonRequest)
        {
            string theResult = PostData(GetRevokeServerActionURL(), jsonRequest);
            return theResult;
        }

        public string SendCancelRequest(string jsonRequest)
        {
            string theResult = PostData(GetCancelServerActionURL(), jsonRequest);
            return theResult;
        }
    }
}
