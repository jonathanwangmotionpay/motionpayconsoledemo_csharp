﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Net;
using System.Security.Cryptography;
using Newtonsoft.Json;

namespace MotionPayConsoleDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            MotionPayData theMotionPayData = new MotionPayData();
            MotionPayAPI theMotionPayAPI = new MotionPayAPI();
            theMotionPayAPI.SetUsingLiveServerFlag(false);

            string scanedAuthcode = "";
            do 
            {
                Console.WriteLine("CAD $0.01 will be charged. Please input the auth code from AliPay(sample 281580286491747888) or WechatPay(sample 134583657133882637):");
                scanedAuthcode = Console.ReadLine().Trim();
            } while (scanedAuthcode.Length <= 0);

            ParamJsonObject theParamJsonObject = new ParamJsonObject
            {
                goods_info = "Test_Product",
                spbill_create_ip = "192.168.2.253",
                store_id = "",
                terminal_no = "MyPosT00001"
            };
            Param theParam = new Param
            {
                amount = 1,                             // price in cents. It means $0.01 in Canadian dollar.
                // authCode = "134583657133882637",     // sample code for testing
                authCode = scanedAuthcode,
                merchantOrderNo = DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                paramJsonObject = theParamJsonObject,
                payChannel = "U"
            };
            Suffix theSuffix = new Suffix
            {
                mid = theMotionPayAPI.getMid()
            };
            OrderRequest theOrderRequset = new OrderRequest
            {
                param = theParam,
                signature = "57351cedc47a8aa71b5405606148f96b6b44974e",   // temp signature from docuemnt
                suffix = theSuffix
            };

            theOrderRequset.signature = theMotionPayData.getSign(theParam, theMotionPayAPI);
            // Serialize it.  
            string serializedJson = JsonConvert.SerializeObject(theOrderRequset);

            // Print on the screen.  
            Console.WriteLine(serializedJson);
            string response = "";
            response = theMotionPayAPI.SendOrderRequest(serializedJson);
            Console.WriteLine("response is:" + response);

            Response responseObj = JsonConvert.DeserializeObject<Response>(response);
            Console.WriteLine("response message: " + responseObj.message);
            if (String.Compare(responseObj.code, "0") == 0)
            {
                Console.WriteLine("The payment request has been sucessfully submitted.");
                Console.WriteLine("The tranLogId is:" + responseObj.result.orderDef.tranLogId);
            }
            else
            {
                Console.WriteLine("There is an error in the response. The error code is: " + responseObj.code);
                Console.Read();
                return;
            }

            Console.WriteLine("Please check your smart phone to finish the payment. After you made the payment, press enter to do the query.");
            Console.Read();


            QueryParam theQueryParam = new QueryParam
            {
                merchantOrderNo = theOrderRequset.param.merchantOrderNo
            };
            QueryRequest theQueryRequest = new QueryRequest
            {
                param = theQueryParam,
                suffix = theSuffix,
                signature = "21264fcf4127bdf77984042456e0c96c6353a5a3"   // temp signature from docuemnt
            };
            theQueryRequest.signature = theMotionPayData.getSign(theQueryParam, theMotionPayAPI);
            // Serialize it.  
            serializedJson = JsonConvert.SerializeObject(theQueryRequest);

            // Print on the screen.  
            Console.WriteLine(serializedJson);
            response = theMotionPayAPI.SendQueryRequest(serializedJson);
            Console.WriteLine("response is:" + response);

            OtherResponse queryResponseObj = JsonConvert.DeserializeObject<OtherResponse>(response);
            Console.WriteLine("response message: " + queryResponseObj.message);
            if (String.Compare(queryResponseObj.code, "0") == 0)
            {
                Console.WriteLine("The query request has been sucessfully submitted.");
                string stateString = "";
                int paymentState = queryResponseObj.result.state;
                stateString = theMotionPayData.GetPaymentStateString(paymentState);
                Console.WriteLine("The state of the payment is:" + stateString);
            }
            else
            {
                Console.WriteLine("There is an error in the response. The error code is: " + responseObj.code);
                return;
            }
            
            // Console.Read();
            // Console.Read();

            string refundCommand = "";
            do
            {
                Console.WriteLine("Enter Yes to refund the payment. Enter No to cancel the payment:");
                refundCommand = Console.ReadLine().Trim();
            } while (String.Compare(refundCommand, "Yes") != 0 && String.Compare(refundCommand, "No") != 0);

            RefundCancelParam theRefundCancelParam = new RefundCancelParam
            {
                orderNo = queryResponseObj.result.orderNo,
                refundAmount = queryResponseObj.result.amount,  // partial refund is supported
                tranCode = queryResponseObj.result.tranCode,
                tranLogId = queryResponseObj.result.tranLogId
            };
            RefundCancelRequest theRefundCancelRequest = new RefundCancelRequest
            {
                param = theRefundCancelParam,
                suffix = theSuffix,
                signature = "21264fcf4127bdf77984042456e0c96c6353a5a3"   // temp signature from docuemnt
            };
            theRefundCancelRequest.signature = theMotionPayData.getSign(theRefundCancelParam, theMotionPayAPI);
            // Serialize it.  
            serializedJson = JsonConvert.SerializeObject(theRefundCancelRequest);
            // Print on the screen.  
            Console.WriteLine(serializedJson);
            string refundOrCancelCommand = "";

            if(String.Compare(refundCommand, "Yes") == 0)
            {
                refundOrCancelCommand = "refund";
                response = theMotionPayAPI.SendRefundRequest(serializedJson);
                Console.WriteLine("response is:" + response);
            }
            else
            {
                refundOrCancelCommand = "cancel";
                response = theMotionPayAPI.SendCancelRequest(serializedJson);
                Console.WriteLine("response is:" + response);
            }

            OtherResponse refundCancelResponseObj = JsonConvert.DeserializeObject<OtherResponse>(response);
            Console.WriteLine("response message: " + queryResponseObj.message);
            if (String.Compare(refundCancelResponseObj.code, "0") == 0)
            {
                Console.WriteLine("The " + refundOrCancelCommand + " request has been sucessfully submitted.");
                if(String.Compare(refundOrCancelCommand,"cancel") == 0)
                {
                    string stateString = "";
                    int paymentState = refundCancelResponseObj.result.state;
                    stateString = theMotionPayData.GetPaymentStateString(paymentState);
                    Console.WriteLine("The state of the payment is:" + stateString);
                }
                else
                {
                    Console.WriteLine("The refund is successfully submitted!");
                }
            }
            else
            {
                Console.WriteLine("There is an error in the response. The error code is: " + responseObj.code);
            }
            Console.Read();
            Console.Read();
        }
    }
}
